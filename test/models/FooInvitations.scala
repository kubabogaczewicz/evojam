package models

trait FooInvitations {
  val dave   = Invitation("dave@foo.com", "Dave Grohl")
  val chris  = Invitation("chris@foo.com", "Chris Shiflett")
  val pat    = Invitation("pat@foo.com", "Pat Smear")
  val taylor = Invitation("taylor@foo.com", "Taylor Hawkins")
  val nate   = Invitation("nate@foo.com", "Nate Mendel")
}
