package models

import org.specs2.mutable

class invitations(invs: Invitation*) extends mutable.Before {
  def before = {
    InvitationDao.init()
    for (inv <- invs) InvitationDao.create(inv)
  }
}
