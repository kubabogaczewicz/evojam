package models

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

import scala.util.{Failure, Success}

@RunWith(classOf[JUnitRunner])
class InvitationDaoSpec extends Specification with FooInvitations {
  sequential

  "inviting dave" should {
    "successfully create an invitation" in new invitations() {
      val old_count = InvitationDao.count(_ => true)
      InvitationDao.create(dave) match {
        case Success(_) => ok
        case _ => ko("Creating invitation fails")
      }
      InvitationDao.count(_ => true) must_== old_count + 1
      InvitationDao.get(dave.email) must beSome(dave)
    }

    "fail on inviting dave twice" in new invitations() {
      val old_count = InvitationDao.count(_ => true)
      InvitationDao.create(dave) match {
        case Success(_) => ok
        case _ => ko("Creating invitation fails")
      }
      InvitationDao.create(dave) match {
        case Failure(DuplicateKeyException()) => ok
        case Success(_) => ko("created second invitation for dave")
        case _ => ko("Oops")
      }
      InvitationDao.count(_ => true) must_== old_count + 1
    }
  }

  "searching for invitations when only the rythm section is invited" should {

    "find invitations for the whole rythm section" in new invitations(nate, taylor) {
      val find_result = InvitationDao.find(_ => true)
      find_result must haveSize(2)
      find_result must contain(nate)
      find_result must contain(taylor)
    }

    "not find dave among invitations" in new invitations(nate, taylor) {
      def byEmail(email: String)(i: Invitation) = i.email == email
      val invitations_of_dave = InvitationDao.find(byEmail(dave.email))
      invitations_of_dave must beEmpty
    }

    "but find nate by name" in new invitations(nate, taylor) {
      val invitations_of_nate = InvitationDao.find(_.invitee.startsWith("Nate"))
      invitations_of_nate must haveSize(1)
      invitations_of_nate must contain(nate)
    }
  }

  "getting invitation" should {
    "get existing invitation" in new invitations(dave) {
      InvitationDao.get(dave.email) must beSome(dave)
    }

    "return none if invitation does not exists" in new invitations(dave,nate) {
      InvitationDao.get(taylor.email) must beNone
    }
  }
}
