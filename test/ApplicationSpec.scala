import controllers.JsonResponses
import models.{InvitationDao, Invitation, FooInvitations}
import org.specs2.execute
import org.specs2.execute.AsResult
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._
import play.api.libs.json.{JsValue, JsPath, Format, Json}

import play.api.test._
import play.api.test.Helpers._

@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification with FooInvitations with JsonResponses[Invitation] {
  sequential

  "Application" should {

    "send 400 bad request on unknown url" in new WithInvitations {
      val req = route(FakeRequest(GET, "/boum"))
      req must beNone
    }

    "send 400 bad request on unparsable json" in new WithInvitations {
      val req = route(FakeRequest(POST, "/invitations"))
      req must beSome

      val response = req.get
      status(response) must equalTo(BAD_REQUEST)
      contentType(response) must beSome.which(_ == "application/json")
      contentAsJson(response) must beEqualTo(jsBadRequest)
    }

  }

  "Invitations" should {
    implicit val invitationFormat: Format[Invitation] = (
      (JsPath \ "email").format[String](email) and
      (JsPath \ "invitee").format[String]
    )(Invitation.apply, unlift(Invitation.unapply))

    "return existing invitation as 200" in new WithInvitations(dave) {
      val req = route(FakeRequest(GET, s"/invitations/${dave.email}"))
      req must beSome

      val response = req.get
      status(response) must equalTo(OK)
      contentType(response) must beSome.which(_ == "application/json")
      contentAsJson(response) must beEqualTo(jsOk(dave))
    }

    "return non-existing invitation as 404" in new WithInvitations(dave) {
      val req = route(FakeRequest(GET, s"/invitations/${taylor.email}"))
      req must beSome

      val response = req.get
      status(response) must equalTo(NOT_FOUND)
      contentType(response) must beSome.which(_ == "application/json")
      contentAsJson(response) must beEqualTo(jsNotFound)
    }

    "return empty invitation list as 200" in new WithInvitations() {
      val req = route(FakeRequest(GET, s"/invitations"))
      req must beSome

      val response = req.get
      status(response) must equalTo(OK)
      contentType(response) must beSome.which(_ == "application/json")
      contentAsJson(response) must beEqualTo(jsOk(Seq[Invitation]()))
    }

    "return filled invitation list as 200" in new WithInvitations(dave, taylor) {
      val req = route(FakeRequest(GET, s"/invitations"))
      req must beSome

      val response = req.get
      status(response) must equalTo(OK)
      contentType(response) must beSome.which(_ == "application/json")
      val fromResponse = (contentAsJson(response) \ "result").as[Seq[Invitation]]
      fromResponse must haveSize(2)
      fromResponse must contain(dave)
      fromResponse must contain(taylor)
    }

    "create new invitation with 201" in new WithInvitations(taylor) {
      val postReq = route(FakeRequest(POST, s"/invitations").withBody(Json.toJson(dave)))
      postReq must beSome

      val response = postReq.get
      status(response) must equalTo(CREATED)
      contentType(response) must beSome.which(_ == "application/json")
      contentAsJson(response) must beEqualTo(jsOk("Invitation created"))

      val getResponse = route(FakeRequest(GET, s"/invitations/${dave.email}")).get
      status(getResponse) must equalTo(OK)
      contentType(getResponse) must beSome.which(_ == "application/json")
      contentAsJson(getResponse) must beEqualTo(jsOk(dave))
    }

    "fail to create second invitation for the same email" in new WithInvitations(dave) {
      val postReq = route(FakeRequest(POST, s"/invitations").withBody(Json.toJson(dave)))
      postReq must beSome

      val response = postReq.get
      status(response) must equalTo(CONFLICT)
      contentType(response) must beSome.which(_ == "application/json")
      contentAsJson(response) must beEqualTo(jsError("Email already registered"))
    }

    "report validation errors in case of wrong email" in new WithInvitations(dave) {
      val postReq = route(FakeRequest(POST, s"/invitations").withJsonBody(Json.obj("email" -> "a", "invitee" -> "a")))
      postReq must beSome

      val response = postReq.get
      status(response) must equalTo(BAD_REQUEST)
      contentType(response) must beSome.which(_ == "application/json")
      private val contentJson: JsValue = contentAsJson(response)
      (contentJson \ "status").asOpt[String] must beSome("Error")
      (contentJson \ "message").asOpt[String] must beSome("Bad request")
      ((contentJson \ "details" \ "obj.email")(0) \ "msg").asOpt[String] must beSome("error.email")
    }

    "report validation errors in case of missing invitee" in new WithInvitations(dave) {
      val postReq = route(FakeRequest(POST, s"/invitations").withJsonBody(Json.obj("email" -> "a")))
      postReq must beSome

      val response = postReq.get
      status(response) must equalTo(BAD_REQUEST)
      contentType(response) must beSome.which(_ == "application/json")
      private val contentJson: JsValue = contentAsJson(response)
      (contentJson \ "status").asOpt[String] must beSome("Error")
      (contentJson \ "message").asOpt[String] must beSome("Bad request")
      ((contentJson \ "details" \ "obj.invitee")(0) \ "msg").asOpt[String] must beSome("error.path.missing")
    }

    "report validation errors in case of missing email" in new WithInvitations(dave) {
      val postReq = route(FakeRequest(POST, s"/invitations").withJsonBody(Json.obj("invitee" -> "a")))
      postReq must beSome

      val response = postReq.get
      status(response) must equalTo(BAD_REQUEST)
      contentType(response) must beSome.which(_ == "application/json")
      private val contentJson: JsValue = contentAsJson(response)
      (contentJson \ "status").asOpt[String] must beSome("Error")
      (contentJson \ "message").asOpt[String] must beSome("Bad request")
      ((contentJson \ "details" \ "obj.email")(0) \ "msg").asOpt[String] must beSome("error.path.missing")
    }
  }

  class WithInvitations(invitations: Invitation*) extends WithApplication {

    override def around[T: AsResult](t: => T): execute.Result = super.around {
      setupData(invitations)
      t
    }

    def setupData(invs: Seq[Invitation]) {
      InvitationDao.init()
      for (i <- invs) InvitationDao.create(i)
    }
  }
}
