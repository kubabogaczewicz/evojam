package controllers

import play.api.libs.json._

trait JsonResponses[T] {
  def jsOk(result: T)(implicit writer: Writes[T]) = jsonResponse("OK")("result")(Json.toJson(result))
  def jsOk(result: Iterable[T])(implicit writer: Writes[T]) = jsonResponse("OK")("result")(Json.toJson(result))
  def jsOk(message: String) = plainResponse("OK")("message")(message)
  def jsBadRequest = error("Bad request")
  def jsConflict = error("Conflict")
  def jsNotFound = error("Not found")

  def jsError = error

  private def error = plainResponse("Error")("message")_
  private def plainResponse(status: String)(resultName: String)(result: String):JsObject = Json.obj("status" -> status, resultName -> result)
  private def jsonResponse(status: String)(resultName: String)(result: JsValue):JsObject = Json.obj("status" -> status, resultName -> result)
}
