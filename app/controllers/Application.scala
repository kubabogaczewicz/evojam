package controllers

import models.{DuplicateKeyException, InvitationDao, Invitation}
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.mvc._

import scala.util.{Failure, Success}

object Application extends Controller with JsonResponses[Invitation] {

  implicit val invitationFormat: Format[Invitation] = (
      (JsPath \ "email").format[String](email) and
      (JsPath \ "invitee").format[String]
    )(Invitation.apply, unlift(Invitation.unapply))

  def get(email: String) = Action {
    InvitationDao.get(email) match {
      case Some(invitation) => Ok(jsOk(invitation))
      case None => NotFound(jsNotFound)
    }
  }

  def find = Action {
    Ok(jsOk(InvitationDao.find(_ => true)))
  }

  def create = Action(parse.json) { implicit request =>
    request.body.validate[Invitation].fold(
      errors => BadRequest(jsBadRequest ++ Json.obj("details" -> JsError.toFlatJson(errors))),
      invitation => InvitationDao.create(invitation) match {
        case Success(_) => Created(jsOk("Invitation created"))
        case Failure(DuplicateKeyException()) => Conflict(jsError("Email already registered"))
        case _ => InternalServerError(jsError("Internal server error"))
      }
    )
  }

}
