package models

import scala.collection.mutable
import scala.util.{Success, Failure, Try}

case class Invitation(email:String, invitee:String)

case class DuplicateKeyException() extends Exception

object InvitationDao {
  private val invitations = mutable.Map[String, Invitation]()

  def init() = invitations.clear()

  def get(email: String):Option[Invitation] = invitations.get(email)

  def find(pred: Invitation => Boolean):Iterable[Invitation] = invitations.filter(e => pred(e._2)).values

  def create(invitation: Invitation):Try[Invitation] = {
    val email = invitation.email
    if (invitations contains email)
      Failure(DuplicateKeyException())
    else
      Success(invitations getOrElseUpdate (email, invitation))
  }

  def count(pred: Invitation => Boolean) = invitations.count(e => pred(e._2))
}
