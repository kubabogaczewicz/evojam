import play.api.GlobalSettings
import play.api.libs.json._
import play.api.mvc._
import play.api.mvc.Results._

import scala.concurrent.Future

object Global extends GlobalSettings {
  override def onBadRequest(request: RequestHeader, error: String) =
    Future.successful(BadRequest(Json.obj(
      "status" -> "Error",
      "message" -> "Bad request"
    )))

  override def onError(request: RequestHeader, ex: Throwable) =
    Future.successful(InternalServerError(Json.obj(
      "status" -> "Error",
      "message" -> "Internal server error"
    )))

  override def onHandlerNotFound(request: RequestHeader) =
    Future.successful(BadRequest(Json.obj(
      "status" -> "Error",
      "message" -> "Bad request"
    )))
}
